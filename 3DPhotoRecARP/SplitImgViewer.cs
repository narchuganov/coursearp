﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _3DPhotoRecARP
{
    public partial class SplitImgViewer : Form
    {
        _3dModel _model;
        private ListViewItem lastItemChecked;
        Label labelId,labelId2;
        public SplitImgViewer(_3dModel model)
        {

            InitializeComponent();
            _model = model;
           
            labelId= new Label();
            labelId.Visible = false;
            labelId.AutoSize = true;
            labelId.Name = "labelId";
            labelId.CreateControl();
            labelId.Parent = pictureBox1;

            labelId2 = new Label();
            labelId2.Visible = false;
            labelId2.AutoSize = true;
            labelId2.Name = "labelId2";
            labelId2.CreateControl();
            labelId2.Parent = pictureBox2;
            
        }

        private void UpdateListPairs()
        {
            listViewPairs.Items.Clear();
            ListViewItem itemList;
            
            foreach (var item in _model.GetPairs)
            {
                itemList = new ListViewItem();
                itemList.Text = item.Key.ToString();
                itemList.SubItems.Add(item.Value.firstPoint.ToString());
                itemList.SubItems.Add(item.Value.secondPoint.ToString());
                
                listViewPairs.Items.Add(itemList);
            }
        }

        private void pictureBox1_LoadCompleted(object sender, AsyncCompletedEventArgs e)
        {
            //pictureBox1.Image = _model.GetObj1.GetImage;
        }

        private void pictureBox2_LoadCompleted(object sender, AsyncCompletedEventArgs e)
        {
            //pictureBox2.Image = _model.GetObj2.GetImage;
        }

        private void SplitImgViewer_Load(object sender, EventArgs e)
        {
            pictureBox1.Image = _model.GetObj1.GetImage;
            pictureBox1.Size = pictureBox1.Image.Size;

            pictureBox2.Image = _model.GetObj2.GetImage;
            pictureBox2.Size = pictureBox2.Image.Size;
            UpdateListPairs();
            UpdateDraw(pictureBox1,pictureBox2);

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            _model.SetPoint(e.Location);
            var g = new Bitmap(pictureBox1.Image);
           
            Graphics draw = Graphics.FromImage(g);
            draw.DrawEllipse(new Pen(Color.Red, 3f), new Rectangle(e.Location, new Size(3, 3)));
            pictureBox1.Image = g;
            UpdateListPairs();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void listViewPairs_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (listViewPairs.CheckedItems.Count != 0)
            {
                ListViewItem item = listViewPairs.CheckedItems[0];
                labelId.Text = item.Text;
                var par = _model.pairs.PairById(Convert.ToInt64(item.Text));
                var p = par.firstPoint;
                p.X += 3;
                p.Y += 3;
                labelId.Location = p;
                labelId.Show();

                if (par.secondPoint != Point.Empty)
                {
                    p = par.secondPoint;
                    p.X += 3;
                    p.Y += 3;
                    labelId2.Text = item.Text;
                    labelId2.Location = p;
                    labelId2.Show();

                }
                else
                    labelId2.Hide();
            }
            else

            {
                labelId.Hide();
                labelId2.Hide();
            }
        }

        private void listViewPairs_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (lastItemChecked != null && lastItemChecked.Checked
        && lastItemChecked != listViewPairs.Items[e.Index])
            {
                // uncheck the last item and store the new one
                lastItemChecked.Checked = false;
            }

            // store current item
            lastItemChecked = listViewPairs.Items[e.Index];
            
        }

        private void UpdateDraw(PictureBox pic, PictureBox pic2)
        {
           
            

            var g = new Bitmap(_model.GetObj1.GetImage);
            var g2 = new Bitmap(_model.GetObj2.GetImage);

            Graphics draw = Graphics.FromImage(g);
            Graphics draw2 = Graphics.FromImage(g2);
            foreach (var item in _model.GetPairs.Values)
            {
                if (pic != null)
                    draw.DrawEllipse(new Pen(Color.Red, 3f), new Rectangle(item.firstPoint, new Size(3, 3)));
                if(!item.secondPoint.IsEmpty)
                    draw2.DrawEllipse(new Pen(Color.Red, 3f), new Rectangle(item.secondPoint, new Size(3, 3)));

            }
            if (pic != null)
                pic.Image = g;
            pic2.Image = g2;
        }

        private void pictureBox2_MouseUp(object sender, MouseEventArgs e)
        {
            if (listViewPairs.CheckedItems.Count != 0)
            {
                _model.SetSecondPoint(e.Location, Convert.ToInt64(listViewPairs.CheckedItems[0].Text));

                var g = new Bitmap(pictureBox2.Image);
                var draw = Graphics.FromImage(g);
                draw.DrawEllipse(new Pen(Color.Red, 3f), new Rectangle(e.Location, new Size(3, 3)));
                pictureBox2.Image = g;                                                 
                UpdateListPairs();
                UpdateDraw(null, pictureBox2);
                labelError.Text = string.Empty;
            }
            else
                labelError.Text = "Выберите идентификатор пары точек справа";
        }

        private void buttonDelPair_Click(object sender, EventArgs e)
        {
            int selected = 0;
            if(listViewPairs.SelectedItems.Count!=0)
            {
                selected = listViewPairs.SelectedIndices[0];
                _model.DeletePoint(Convert.ToInt64(listViewPairs.SelectedItems[0].Text));                
                UpdateDraw(pictureBox1,pictureBox2);      
                UpdateListPairs();
                if ((selected-1)>=0)
                {
                    listViewPairs.Items[selected - 1].Selected = true;
                    listViewPairs.Focus();
                    
                }
            }
        }

        private void listViewPairs_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void button2_Click(object sender, EventArgs e)
        {
            pictureBox1.Refresh();
        }
    }
}
