﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using _3DPhotoRecARP;
namespace _3DPhotoRecARP
{
    public partial class ImgViewer : Form
    {
        ImgObject _obj;
        public ImgViewer(ImgObject obj)
        {
            InitializeComponent();

            _obj = obj;
        }
        
        private void ImgViewer_Load(object sender, EventArgs e)
        {
            //pictureBox.Image = _obj.GetImage;
            UpdatePoints(_obj,Point.Empty);
            pictureBox.Size = pictureBox.Image.Size;
            listViewPoints.Height = pictureBox.Size.Height;
            
        }

        private void button_ok_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_clearP_Click(object sender, EventArgs e)
        {
            //_obj.ClearPoints();
            UpdatePoints(_obj,Point.Empty);
        }

        private void button_delast_Click(object sender, EventArgs e)
        {
            //_obj.DeleteLastAddedPoint();
            UpdatePoints(_obj,Point.Empty);
        }


        private void UpdatePoints(ImgObject obj, Point point)
        {/*
            if (obj.SetPoint(point))
            {
                Bitmap img = new Bitmap(pictureBox.Image);
                Graphics draw = Graphics.FromImage(img);
                draw.DrawEllipse(new Pen(Color.Red, 3f), new Rectangle(obj.GetPoints[obj.GetPoints.Count-1], new Size(3, 3)));
                listViewPoints.Items.Add((obj.GetPoints.Count-1).ToString(), obj.GetPoints[obj.GetPoints.Count-1].ToString(), 0);
                pictureBox.Image = img;
            }
            else
            {
                Bitmap img = new Bitmap(obj.GetImage);
                Graphics draw = Graphics.FromImage(img);
                listViewPoints.Items.Clear();

                for (int i = 0; i < obj.GetPoints.Count; i++)
                {
                    draw.DrawEllipse(new Pen(Color.Red, 3f), new Rectangle(obj.GetPoints[i], new Size(3, 3)));
                    listViewPoints.Items.Add(i.ToString(), obj.GetPoints[i].ToString(), 0);
                }
                pictureBox.Image = img;
            }
          */
        }

        private void pictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            //MessageBox.Show("coord:",e.Location.ToString(), MessageBoxButtons.OK);
            
            UpdatePoints(_obj,e.Location);
        }

        private void pictureBox_Paint(object sender, PaintEventArgs e)
        {
           
        }

      
    }
}
