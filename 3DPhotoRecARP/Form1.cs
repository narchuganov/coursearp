﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using _3DPhotoRecARP;
using OpenCvSharp;
using CPI.Plot3D;

namespace _3DPhotoRecARP
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }
      
        _3dModel model;
        bool calced;
        private void button_openImage1_Click(object sender, EventArgs e)
        {
             
            var dr = openImageDialog.ShowDialog();
            xixi:
            if (dr == System.Windows.Forms.DialogResult.OK && openImageDialog.FileNames.Length==2)
            {
                try {
                    var bmp1 = new Bitmap(openImageDialog.FileNames[0]);
                    var bmp2 = new Bitmap(openImageDialog.FileNames[1]);
                    if ((bmp1.Height!=bmp2.Height)&&(bmp1.Width!=bmp2.Width))
                    {
                       if ( MessageBox.Show("Размер изображений не эквивалентен", "Невозможное использование", MessageBoxButtons.OK)== DialogResult.OK)
                        {
                            goto xixi;
                        }
                    }
    
                    model = new _3dModel(bmp1, bmp2, openImageDialog.FileNames[0], openImageDialog.FileNames[1]);
                    pictureBox_img1.Image = model.GetObj1.GetImage;
                    pictureBox_img2.Image = model.GetObj2.GetImage;
                    textBox_img1.Text = model.GetInfo();
                    calced = false;
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
                }
                 
                
            }
        }

        private void button_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox_img1_Click(object sender, EventArgs e)
        {
            if (model != null)
            {
                SplitImgViewer viewer = new SplitImgViewer(model);
                viewer.ShowDialog();
            }
        }

        private void pictureBox_img2_Click(object sender, EventArgs e)
        {
            if (model != null)
            {
                SplitImgViewer viewer = new SplitImgViewer(model);
                viewer.ShowDialog();
            }
        }

        private void buttonSetPoints_Click(object sender, EventArgs e)
        {
            if (model!= null)
            {
                SplitImgViewer viewer = new SplitImgViewer(model);
                viewer.ShowDialog();
            }
             
        }

        private void test()
        {
        
            int focal = Convert.ToInt16(textBoxfocal.Text), Baseline = Convert.ToInt16(textBoxBaseline.Text);
            int w = model.GetObj1.GetImage.Width;
            int h = model.GetObj2.GetImage.Height;
            int x = 0, z = 0, y = 0;
            foreach (var item in model.GetPairs)
            {
               // var delta = Math.Sqrt(Math.Pow(item.Value.secondPoint.X - item.Value.firstPoint.X,2f) +Math.Pow(item.Value.secondPoint.Y - item.Value.firstPoint.Y,2f));
               var delta = item.Value.firstPoint.X - item.Value.secondPoint.X;
                
                z =( Baseline * focal) / Convert.ToInt16(delta);
                x = (z * (item.Value.firstPoint.X - w / 2)) / focal;
                y = (z * (item.Value.firstPoint.Y - h / 2)) / focal;
                item.Value.PointThreeD = new Point3D(x,y,z);
            }
            calced = true;
        }

        private void buttonCalcModel_Click(object sender, EventArgs e)
        {
            if (model != null)
                if (model.pairs.GetIds().Length > 0)
                    test();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void buttonViewModel_Click(object sender, EventArgs e)
        {
            if (calced)
            {
                ViewModel viewer = new ViewModel(model);
                viewer.ShowDialog();
            }
        }

        private void textBoxfocal_KeyDown(object sender, KeyEventArgs e)
        {
            // Initialize the flag to false.
            nonNumberEntered = false;

            // Determine whether the keystroke is a number from the top of the keyboard.
            if (e.KeyCode < Keys.D0 || e.KeyCode > Keys.D9)
            {
                // Determine whether the keystroke is a number from the keypad.
                if (e.KeyCode < Keys.NumPad0 || e.KeyCode > Keys.NumPad9)
                {
                    // Determine whether the keystroke is a backspace.
                    if (e.KeyCode != Keys.Back)
                    {
                        // A non-numerical keystroke was pressed.
                        // Set the flag to true and evaluate in KeyPress event.
                        nonNumberEntered = true;
                    }
                }
            }
            //If shift key was pressed, it's not a number.
            if (Control.ModifierKeys == Keys.Shift)
            {
                nonNumberEntered = true;
            }
        }
        bool nonNumberEntered;
        private void textBoxBaseline_KeyDown(object sender, KeyEventArgs e)
        {// Initialize the flag to false.
            nonNumberEntered = false;

            // Determine whether the keystroke is a number from the top of the keyboard.
            if (e.KeyCode < Keys.D0 || e.KeyCode > Keys.D9)
            {
                // Determine whether the keystroke is a number from the keypad.
                if (e.KeyCode < Keys.NumPad0 || e.KeyCode > Keys.NumPad9)
                {
                    // Determine whether the keystroke is a backspace.
                    if (e.KeyCode != Keys.Back)
                    {
                        // A non-numerical keystroke was pressed.
                        // Set the flag to true and evaluate in KeyPress event.
                        nonNumberEntered = true;
                    }
                }
            }
            //If shift key was pressed, it's not a number.
            if (Control.ModifierKeys == Keys.Shift)
            {
                nonNumberEntered = true;
            }
        }

        private void textBoxfocal_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxfocal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (nonNumberEntered)
                e.Handled = true;
        }

        private void textBoxfocal_KeyUp(object sender, KeyEventArgs e)
        {
            
        }

        private void textBoxBaseline_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (nonNumberEntered)
                e.Handled = true;
        }
    }
}
