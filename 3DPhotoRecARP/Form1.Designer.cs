﻿namespace _3DPhotoRecARP
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.openImageDialog = new System.Windows.Forms.OpenFileDialog();
            this.button_openImage = new System.Windows.Forms.Button();
            this.groupBox_img1 = new System.Windows.Forms.GroupBox();
            this.buttonSetPoints = new System.Windows.Forms.Button();
            this.pictureBox_img2 = new System.Windows.Forms.PictureBox();
            this.textBox_img1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox_img1 = new System.Windows.Forms.PictureBox();
            this.button_exit = new System.Windows.Forms.Button();
            this.buttonCalcModel = new System.Windows.Forms.Button();
            this.buttonViewModel = new System.Windows.Forms.Button();
            this.textBoxfocal = new System.Windows.Forms.TextBox();
            this.textBoxBaseline = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox_img1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_img2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_img1)).BeginInit();
            this.SuspendLayout();
            // 
            // openImageDialog
            // 
            this.openImageDialog.Filter = "JPEG|*.jpg;*.jpeg|PNG|*.png|BMP|*.bmp";
            this.openImageDialog.Multiselect = true;
            this.openImageDialog.RestoreDirectory = true;
            this.openImageDialog.Title = "Загрузить изображение";
            // 
            // button_openImage
            // 
            this.button_openImage.Location = new System.Drawing.Point(6, 19);
            this.button_openImage.Name = "button_openImage";
            this.button_openImage.Size = new System.Drawing.Size(122, 23);
            this.button_openImage.TabIndex = 0;
            this.button_openImage.Text = "Загрузить";
            this.button_openImage.UseVisualStyleBackColor = true;
            this.button_openImage.Click += new System.EventHandler(this.button_openImage1_Click);
            // 
            // groupBox_img1
            // 
            this.groupBox_img1.Controls.Add(this.label3);
            this.groupBox_img1.Controls.Add(this.label2);
            this.groupBox_img1.Controls.Add(this.textBoxBaseline);
            this.groupBox_img1.Controls.Add(this.textBoxfocal);
            this.groupBox_img1.Controls.Add(this.buttonCalcModel);
            this.groupBox_img1.Controls.Add(this.buttonSetPoints);
            this.groupBox_img1.Controls.Add(this.pictureBox_img2);
            this.groupBox_img1.Controls.Add(this.textBox_img1);
            this.groupBox_img1.Controls.Add(this.label1);
            this.groupBox_img1.Controls.Add(this.pictureBox_img1);
            this.groupBox_img1.Controls.Add(this.button_openImage);
            this.groupBox_img1.Location = new System.Drawing.Point(12, 12);
            this.groupBox_img1.Name = "groupBox_img1";
            this.groupBox_img1.Size = new System.Drawing.Size(860, 336);
            this.groupBox_img1.TabIndex = 1;
            this.groupBox_img1.TabStop = false;
            this.groupBox_img1.Text = "Подготовка";
            // 
            // buttonSetPoints
            // 
            this.buttonSetPoints.Location = new System.Drawing.Point(535, 250);
            this.buttonSetPoints.Name = "buttonSetPoints";
            this.buttonSetPoints.Size = new System.Drawing.Size(132, 23);
            this.buttonSetPoints.TabIndex = 7;
            this.buttonSetPoints.Text = "Расставить точки";
            this.buttonSetPoints.UseVisualStyleBackColor = true;
            this.buttonSetPoints.Click += new System.EventHandler(this.buttonSetPoints_Click);
            // 
            // pictureBox_img2
            // 
            this.pictureBox_img2.Location = new System.Drawing.Point(535, 19);
            this.pictureBox_img2.Name = "pictureBox_img2";
            this.pictureBox_img2.Size = new System.Drawing.Size(290, 225);
            this.pictureBox_img2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_img2.TabIndex = 1;
            this.pictureBox_img2.TabStop = false;
            this.pictureBox_img2.Click += new System.EventHandler(this.pictureBox_img2_Click);
            // 
            // textBox_img1
            // 
            this.textBox_img1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox_img1.Location = new System.Drawing.Point(9, 62);
            this.textBox_img1.Multiline = true;
            this.textBox_img1.Name = "textBox_img1";
            this.textBox_img1.ReadOnly = true;
            this.textBox_img1.Size = new System.Drawing.Size(119, 182);
            this.textBox_img1.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 45);
            this.label1.MaximumSize = new System.Drawing.Size(122, 200);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Информация:";
            // 
            // pictureBox_img1
            // 
            this.pictureBox_img1.Location = new System.Drawing.Point(198, 19);
            this.pictureBox_img1.Name = "pictureBox_img1";
            this.pictureBox_img1.Size = new System.Drawing.Size(290, 225);
            this.pictureBox_img1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_img1.TabIndex = 1;
            this.pictureBox_img1.TabStop = false;
            this.pictureBox_img1.Click += new System.EventHandler(this.pictureBox_img1_Click);
            // 
            // button_exit
            // 
            this.button_exit.Location = new System.Drawing.Point(797, 354);
            this.button_exit.Name = "button_exit";
            this.button_exit.Size = new System.Drawing.Size(75, 23);
            this.button_exit.TabIndex = 2;
            this.button_exit.Text = "Выход";
            this.button_exit.UseVisualStyleBackColor = true;
            this.button_exit.Click += new System.EventHandler(this.button_exit_Click);
            // 
            // buttonCalcModel
            // 
            this.buttonCalcModel.Location = new System.Drawing.Point(710, 307);
            this.buttonCalcModel.Name = "buttonCalcModel";
            this.buttonCalcModel.Size = new System.Drawing.Size(115, 23);
            this.buttonCalcModel.TabIndex = 8;
            this.buttonCalcModel.Text = "Рассчитать модель";
            this.buttonCalcModel.UseVisualStyleBackColor = true;
            this.buttonCalcModel.Click += new System.EventHandler(this.buttonCalcModel_Click);
            // 
            // buttonViewModel
            // 
            this.buttonViewModel.Location = new System.Drawing.Point(547, 354);
            this.buttonViewModel.Name = "buttonViewModel";
            this.buttonViewModel.Size = new System.Drawing.Size(132, 23);
            this.buttonViewModel.TabIndex = 9;
            this.buttonViewModel.Text = "Просмотреть модель";
            this.buttonViewModel.UseVisualStyleBackColor = true;
            this.buttonViewModel.Click += new System.EventHandler(this.buttonViewModel_Click);
            // 
            // textBoxfocal
            // 
            this.textBoxfocal.Location = new System.Drawing.Point(710, 252);
            this.textBoxfocal.Name = "textBoxfocal";
            this.textBoxfocal.Size = new System.Drawing.Size(115, 20);
            this.textBoxfocal.TabIndex = 10;
            this.textBoxfocal.Text = "30";
            this.textBoxfocal.TextChanged += new System.EventHandler(this.textBoxfocal_TextChanged);
            this.textBoxfocal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxfocal_KeyDown);
            this.textBoxfocal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxfocal_KeyPress);
            this.textBoxfocal.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBoxfocal_KeyUp);
            // 
            // textBoxBaseline
            // 
            this.textBoxBaseline.Location = new System.Drawing.Point(710, 281);
            this.textBoxBaseline.Name = "textBoxBaseline";
            this.textBoxBaseline.Size = new System.Drawing.Size(115, 20);
            this.textBoxBaseline.TabIndex = 11;
            this.textBoxBaseline.Text = "29";
            this.textBoxBaseline.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxBaseline_KeyDown);
            this.textBoxBaseline.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxBaseline_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(673, 255);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Focal:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(673, 284);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "B:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 389);
            this.Controls.Add(this.button_exit);
            this.Controls.Add(this.groupBox_img1);
            this.Controls.Add(this.buttonViewModel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "3DRecARP";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.groupBox_img1.ResumeLayout(false);
            this.groupBox_img1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_img2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_img1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openImageDialog;
        private System.Windows.Forms.Button button_openImage;
        private System.Windows.Forms.GroupBox groupBox_img1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox_img1;
        private System.Windows.Forms.PictureBox pictureBox_img2;
        private System.Windows.Forms.TextBox textBox_img1;
        private System.Windows.Forms.Button button_exit;
        private System.Windows.Forms.Button buttonSetPoints;
        private System.Windows.Forms.Button buttonCalcModel;
        private System.Windows.Forms.Button buttonViewModel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxBaseline;
        private System.Windows.Forms.TextBox textBoxfocal;
    }
}

