﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
//using CPI.Plot3D;

namespace _3DPhotoRecARP
{


    public class Point3D
    {
        private int _x;
        private int _y;
        private int _z;
        public int X
        {
            get { return _x; }
        }
        public int Y
        {
            get { return _y; }
        }

        public int Z
        {
            get { return _z; }
        }
        public Point3D(int x, int y, int z)
        {
            _x = x;
            _y = y;
            _z = z;
        }
    }
    

   /// <summary>
   /// Представляет точку
   /// </summary>
    public class Pair
    {
        private Point _firstPoint;
        private Point _secondPoint;
        private long _pointId;
        private Point3D _point3d;
        /// <summary>
        /// Создаёт новую пару точек
        /// </summary>
        /// <param name="firstPoint">Первая точка</param>
        /// <param name="secondPoint">Вторая точка</param>
        /// <param name="id">Идентификатор точки</param>
        public Pair(Point firstPoint, Point secondPoint, long id)
        {
            this._pointId = id;
            this._firstPoint = firstPoint;
            this._secondPoint = secondPoint;
        }

        

        public Point firstPoint
        {
            get { return _firstPoint; }
            set { _firstPoint = value; }
        }

       /* public Point3D PointThreeD
        {
            get { return _point3d; }
            set { _point3d = value; }
        }*/
        public Point3D PointThreeD
        {
            get { return _point3d; }
            set { _point3d = value; }
        }

        public Point secondPoint
        {
            get { return _secondPoint; }
            set { _secondPoint = value; }
        }
        public long Id
        {
            get { return _pointId; }
        }

    }    

    /// <summary>
    /// Класс хранящий и создающий пары точек, делает это согласованно, создавая уникальный идентификатор.
    /// </summary>
    public class PairObjects
    {
       
        Dictionary<long, Pair> _IdedPairs;

        public Dictionary<long,Pair> IdedPair
        {
            get
            {
                return new Dictionary<long, Pair>(_IdedPairs);
            }
        }

        public PairObjects()
        {
            _IdedPairs = new Dictionary<long, Pair>();
        }
            
        

        /// <summary>
        /// генерирует идентификатор пары точек, используются координаты первой, ведущей точки.
        /// </summary>
        /// <param name="coords"></param>
        /// <returns></returns>
        private long GenerateId(Point coords)
        {
            uint idhelper = 0;
            long newID = (Convert.ToInt64(coords.X) + Convert.ToInt64(coords.Y)) * 4;
            while (_IdedPairs.ContainsKey(newID))
            {
                idhelper++;
                newID += idhelper;
            }
            return newID;
        }

        /// <summary>
        /// Создаёт пару точек в словаре точек, согласовывая ID, координаты второй точки пустые. 
        /// </summary>
        /// <param name="coordinates">Координата первой точки</param>
        /// <returns>Идентификатор новой пары точек</returns>
        public long AddPoint(Point coordinates)
        {
            Pair pair = new Pair(coordinates, Point.Empty,GenerateId(coordinates));
            _IdedPairs.Add(pair.Id, pair);
            return pair.Id;
        }

        public bool Set3DPoint(int x, int y, int z, long PairId)
        {
            if (_IdedPairs.ContainsKey(PairId))
            {
                var pair = _IdedPairs[PairId].PointThreeD = new Point3D(x,y,z);
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Создаёт пару точек в словаре точек, согласовывая ID
        /// </summary>
        /// <param name="coordinatesFirst">Координата первой точки</param>
        /// <param name="coordinatesSecond">Координата второй точки</param>
        /// <returns></returns>
        public long AddPoint(Point coordinatesFirst, Point coordinatesSecond)
        {
            Pair pair = new Pair(coordinatesFirst, coordinatesSecond, GenerateId(coordinatesFirst));
            _IdedPairs.Add(pair.Id, pair);
            return pair.Id;
        }

        /// <summary>
        /// Удаляет пару точек по заданному идентификатору
        /// </summary>
        /// <param name="PointId">Идентификатор пары</param>
        /// <returns>успех удаления</returns>
        public bool DeletePair(long PairId)
        {
            if (_IdedPairs.ContainsKey(PairId))
            {
                _IdedPairs.Remove(PairId);
                return true;
            }
            else
                return false;
        }


        public void ClearPairs()
        {
            _IdedPairs.Clear();
        }

        public Point FirstPointById(long PairId)
        {
            if (_IdedPairs.ContainsKey(PairId))
            {
                return _IdedPairs[PairId].firstPoint;
            }
            else
                throw new KeyNotFoundException("A pair with specified id are not present");
        }

        public Point SecondPointById(long PairId)
        {
            if (_IdedPairs.ContainsKey(PairId))
            {
                return _IdedPairs[PairId].secondPoint;
            }
            else
                throw new KeyNotFoundException("A pair with specified id are not present");
        }

        public long[] GetIds()
        {
           
            return _IdedPairs.Keys.ToArray<long>();
        }

        public Pair PairById(long PairId)
        {
            if (_IdedPairs.ContainsKey(PairId))
            {
                return _IdedPairs[PairId];
            }
            else
                throw new KeyNotFoundException("A pair with specified id are not present");
        }

        /// <summary>
        /// Добавляет  или изменяет координаты второй точки в паре точек
        /// </summary>
        /// <param name="PairId">Идентификатор пары</param>
        /// <param name="coordinates">Координаты второй точки</param>
        /// <returns>Успех операции</returns>
        public bool ChangeSecondPointById(long PairId, Point coordinates)
        {
            if (_IdedPairs.ContainsKey(PairId))
            {
                var pair = _IdedPairs[PairId].secondPoint = coordinates;
                return true;
            }
            else
                return false;
        }


        /// <summary>
        /// Изменияет координаты первой точки в паре точек
        /// </summary>
        /// <param name="PairId">Идентификатор пары</param>
        /// <param name="coordinates">Координаты первой точки</param>
        /// <returns>Успех изменения</returns>
        public bool ChangeFirstPointById(long PairId, Point coordinates)
        {
            if (_IdedPairs.ContainsKey(PairId))
            {
                var pair = _IdedPairs[PairId].firstPoint = coordinates;
                return true;
            }
            else
                return false;
        }


    }








    public class ImgObject
    {
        Bitmap _image;
       // List<Point> _points;
        string _info;
        public Bitmap GetImage
        {
            get { return _image; }
        }

     /*   public void ClearPoints()
        {
            _points.Clear();
        }
        
        public void DeleteLastAddedPoint()
        {

            if (_points.Count>=1)
                _points.RemoveAt(_points.Count-1);
        }
        */
        public string GetInfo
        {
            get
            {
                return _info;
            }
        }
        
        public Size GetSize
        {
            get
            {
                return _image.Size;
            }
        }

      /*  public Point SetPoint
        {
            set
            {
                bool exist = false;
                for (int i = 0; i < _points.Count; i++)
                {
                    if (value == _points[i])
                    {
                        exist = true;
                    }
                }
                if (!exist)
                    _points.Add(value);
            }
        }*/

        /*public bool SetPoint(Point point)
        {
            if (point != Point.Empty)
            {
                for (int i = 0; i < _points.Count; i++)
                {
                    if (point == _points[i])
                    {
                        return false;
                    }
                }
                _points.Add(point);
                return true;
            }
            else
                return false;
        }

        public List<Point> GetPoints
        {
            get
            {
                return _points;
            }
        }
        */
        public ImgObject(Bitmap image,string fileinfo)
        {
            _image = image;
            _info = "\n\n"+"Имя: " + fileinfo + "\n"+"Разрешение: " + image.Size.ToString()+"\n";
        }
    }

    public enum ImageIndex {Image1=1,Image2=2};

    public class _3dModel
    {

        ImgObject _image1;
        ImgObject _image2;

        public   PairObjects pairs;

        

        public _3dModel(Bitmap image1, Bitmap image2,string fileinfo_1, string fileinfo_2)
        {
            _image1 = new ImgObject(image1,fileinfo_1);
            _image2 = new ImgObject(image2, fileinfo_2);
            pairs = new PairObjects();
        }

        

        public long SetPoint(Point point)
        {


           return pairs.AddPoint(point);
            

        }

        public void ChangeFirstPoint(Point point, long ID)
        {


          pairs.ChangeFirstPointById(ID, point);


        }


        public void SetSecondPoint(Point point,long ID)
        {


            pairs.ChangeSecondPointById(ID, point);


        }


        public Dictionary<long,Pair> GetPairs
        {
            get
            {
                return pairs.IdedPair;
            }
        }



        public ImgObject GetObj1
        {
            get
            {
                return _image1;
            }
        }
        public ImgObject GetObj2
        {
            get
            {
                return _image2;
            }
        }




        public void ClearAllPoints()
        {
            pairs.ClearPairs();
        }
     

        public void ClearPoints(ImageIndex imgindex)
        {
            if (imgindex == ImageIndex.Image1)
            {
                foreach (var item in pairs.GetIds())
                {
                    pairs.ChangeFirstPointById(item, Point.Empty);
                }

            }
            else
                foreach (var item in pairs.GetIds())
                {
                    pairs.ChangeSecondPointById(item, Point.Empty);
                }
        }

        public void DeleteLastAddedPoint(ImageIndex imgindex)
        {
            if (imgindex == ImageIndex.Image1)
            {

            }
            else
                ;
        }

        public void DeletePoint(long Id)
        {
            pairs.DeletePair(Id);
        }
        public string GetInfo()
        {
            return _image1.GetInfo + "\n" + _image2.GetInfo;
        }
    }
}
