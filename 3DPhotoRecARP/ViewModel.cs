﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CPI.Plot3D;

namespace _3DPhotoRecARP
{
    public partial class ViewModel : Form
    {
        public ViewModel(_3dModel model)
        {
            InitializeComponent();
            this.model = model;
        }
        _3dModel model;
        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void ViewModel_Load(object sender, EventArgs e)
        {
            pictureBox1.Size = model.GetObj1.GetSize;
            pictureBox2.Size = model.GetObj2.GetSize;
            var btm = new Bitmap(model.GetObj1.GetSize.Width, model.GetObj1.GetSize.Height);
            var btm2 = new Bitmap(model.GetObj1.GetSize.Width, model.GetObj1.GetSize.Height);
            var g2 = Graphics.FromImage(btm2);
            var g = Graphics.FromImage(btm);
            var centerx = model.GetObj1.GetSize.Width / 2;
            var centery = model.GetObj1.GetSize.Height / 2;

            g.DrawLine(new Pen(Color.Black, 2f), new Point(centerx, 0), new Point(centerx, model.GetObj1.GetSize.Height));
            g.DrawLine(new Pen(Color.Black, 2f), new Point(0, centery), new Point(model.GetObj1.GetSize.Width, centery));

            g2.DrawLine(new Pen(Color.Black, 2f), new Point(centerx, 0), new Point(centerx, model.GetObj1.GetSize.Height));
            g2.DrawLine(new Pen(Color.Black, 2f), new Point(0, centery), new Point(model.GetObj1.GetSize.Width, centery));
            foreach (var item in model.GetPairs)
            {
                g.DrawEllipse(new Pen(Color.Red, 3f), new Rectangle(new Point(item.Value.PointThreeD.X + centerx, item.Value.PointThreeD.Y+centery), new Size(3, 3)));
                g2.DrawEllipse(new Pen(Color.Red, 3f), new Rectangle(new Point(item.Value.PointThreeD.X + centerx, item.Value.PointThreeD.Z + centery), new Size(3, 3)));
            }
            pictureBox1.Image = btm;
            pictureBox2.Image = btm2;
        
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {
           
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
